# Word Order 1

=> ./index.gmi	Back to Kašte page.

The basic word order is SOV.
This means that the subject comes first, the object comes next, and the verb comes last.
This is different than English which is SVO, where the verb goes in between the subject and the object.

Take this example:
I have a rock.

In this sentence, the subject is "I", the thing __doing__ the verb.
The object is "rock", the thing the verb is being done __to__.
"Have" is the verb, the action being done.

The same sentence in Ijeða would be:
Sa om anoð.

"Sa" means "I".
"Om" means "rock".
"Anoð" means "have".
Notice how the verb goes after the object, letting the sentence follow the structure: subject, object, then verb.
Also notice that the word "a" is missing. Ijeða doesn't have words for "a" and "the". You don't have to worry about this now, I will talk about it more later.

I will keep going with a couple more examples.

You are playing music.
Subject - You, Verb - Are playing, Object - Music
In Ijeða:
Ja karauš čazly.
Ja - You, Karauš - Music, Čazly - Play

He is baking cake.
Subject - He, Verb - Is cooking/baking, Object - Cake.
In Ijeða:
Seð kužum hasir.
Seð - He/She, Kužum - Cake, Hasir - cook/bake

