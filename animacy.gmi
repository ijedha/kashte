# Animacy? What's that?

=> /kashte Back to Kašte page

You might know that some languages split their verbs into masculine and feminine, such as in Spanish and French.

Nouns in Euðeň are split into two groups:  Animate, and Inanimate.

In Euðeň, animacy is defined as whether or not something is alive.

It is really easy to tell if a noun is animate, all you have to do is check the last letter:

If the noun ends in a vowel, then it's animate.
This is called an "open" word.
Anku    Fish
Kemi    Plant
Kowo    Dragon
Ona     Squid

If the noun ends in a consonant, then it's inanimate.
This is called a "closed" word.
Om      Rock
Has     Bowl
Kužum   Cake

However, this creates a problem. If you're served chicken at a restaurant, the chicken is very much not alive! What happens to words that can be both animate and inanimate?

## Converting nouns

To convert an inanimate noun to an animate noun, we have to "open" the word by repeating the last vowel again after the last consonant. This isn’t used often, and so these examples don't make a lot of sense.
Om      -> Omo
Has     -> Hasa
Kužum   -> Kužumu

To convert an animate noun to an inanimate noun, there is a set of vowel and consonant pairs:
* A - Š
* E - Ž
  
* O - Þ
* Y - Ň
* U - Ð
  
* I - S

To "close" the word, you take the last vowel and use the consonant from its pair.
Anku ->	Ankuð
Kemi ->	Kemis
Kowo ->	Kowoþ
Ona ->	Onaš

Since these pairs are used to "close" a word, they are called Closed Noun pairs, or just CN for short.

These letter pairs are also used for something else later, so it's good to remember them.
