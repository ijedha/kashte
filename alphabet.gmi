#Alphabet and Spelling

=> ./index.gmi	Back to Kašte page

Spelling in Euðeň is very simple.
Words are pronounced as they are written, and written as they sound.
All letters are always pronounced the same, except for one letter which has a couple (equally correct) ways pronounciations.

## Standard Spelling

### Consonants

    h /h/ - home
    m /m/ - mom
    n /n/ - name
    ň /ŋ/ - ng in wing
    t /t/ - tail
    þ /θ/ - think
    ð /ð/ - the
    s /s/ - sing
    z /z/ - zip
    š /ʃ/ - share
    ž /ʒ/ - si in vision
    č /t͡ʃ/- chair
    l /l/ - leaf
    w /w/ - with
    r /ɾ/ - like the "tt" in "butter" in some accents
    k /k/ - king
    p /p/ - pet
    j /j/ - yet

### Vowels

    i /i/ - tee
    o /o/ - cold
    e /e/ - similar to "e" in spanish
    y /ɯ~ə/ - ə like in crumb, ɯ has no english equivalent
    u /u/ - blue
    a /a/ - father

## Alternate spellings

Of course, not everyone has all of these keys on their keyboard (I use a custom layout).

### The official compatibility spelling

Everything is the same as the standard spelling, except for the following letters:
þ -> th
ð -> dh OR d
š -> sh
ž -> zh
č -> ch OR c

"ð" and "č" can be shortened to just "d" and "c" because those letters aren't used in the standard spelling anyway, and they're easier to type.

"d" and "c" are often used instead of "ð" and "č" in the standard spelling, while still using the characters "þ, š, ž".
